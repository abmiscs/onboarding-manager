FROM node:12-alpine as builder

WORKDIR /app

COPY ./package*.json ./
COPY ./.env.staging ./

RUN npm install

COPY . .
RUN npm install -g env-cmd
RUN npm run build:staging

FROM nginx

EXPOSE 3000
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/build /usr/share/nginx/html
# COPY ./build /usr/share/nginx/html