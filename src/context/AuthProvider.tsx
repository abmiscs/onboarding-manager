import React from 'react'

type Action =
    { type: 'login', payload: { authtoken: string } }
    | { type: 'logout' }

type Dispatch = (action: Action) => void
type AuthProviderProps = { children: React.ReactNode }

const initialState: any = { authenticated: false, authtoken: null }

const AuthStateContext = React.createContext<any>(initialState);
const AuthDispatchContext = React.createContext<Dispatch | undefined>(undefined);

function authReducer(state: any, action: Action) {
    switch (action.type) {
        case 'login': {
            return {
                ...state,
                authenticated: true,
                authtoken: action.payload.authtoken
            }
        }
        case 'logout': {
            return {
                ...state,
                authenticated: false,
                authtoken: null
            }
        }
        default: {
            return state;
        }
    }
}

function AuthProvider({ children }: AuthProviderProps) {
    const [state, dispatch] = React.useReducer(authReducer, initialState)
    return (
        <AuthStateContext.Provider value={state}>
            <AuthDispatchContext.Provider value={dispatch}>
                {children}
            </AuthDispatchContext.Provider>
        </AuthStateContext.Provider>
    )
}

function AuthCosumer({ children }: any) {
    return (
        <AuthStateContext.Consumer>
            {context => {
                if (context === undefined) {
                    throw new Error('ThemeConsumer must be used within a ThemeProvider')
                }
                return children(context)
            }}
        </AuthStateContext.Consumer>
    )
}

function useAuthState() {
    const context = React.useContext(AuthStateContext)
    if (context === undefined) {
        throw new Error('useAuthState must be used within a AuthProvider')
    }
    return context
}

function useAuthDispatch() {
    const context = React.useContext(AuthDispatchContext)
    if (context === undefined) {
        throw new Error('useAuthDispatch must be used within a AuthProvider')
    }
    return context
}

function useAuthProvider() {
    return [useAuthState(), useAuthDispatch()]
}

export { AuthProvider, AuthCosumer, useAuthState, useAuthDispatch, useAuthProvider }