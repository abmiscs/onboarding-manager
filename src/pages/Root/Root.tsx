import React, { useEffect } from 'react'
import { Switch, Route, useHistory } from 'react-router-dom'

import { useAuthState } from '../../context/AuthProvider'

import Dashboard from '../Dashboard/Dashboard'
import Authentication from '../Authentication/Authentication'

function Root(props: any) {

    const auth = useAuthState();

    const history = useHistory();

    // useEffect(() => {
    //     if (!auth.authenticated || !auth.authtoken) {
    //         history.push('/auth');
    //     } else {
    //         history.push('/');
    //     }
    // }, [auth])

    return (
        <Switch>
            <Route path="/auth" component={Authentication} />
            <Route path="/" component={Dashboard} />
        </Switch>
    )

}

export default Root;

// <Route path="/login" component={Login} />
// <Route path="/" exact component={WaitingComponent(Landing)} />
// <Route path="/details/:sessionId" component={WaitingComponent(Details)} />