import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom';

import SidebarContainer from '../../components/UI/SidebarContainer/SidebarContainer';

import Sessions from '../../components/Dashboard/Sessions/Sessions';
import User from '../../components/Dashboard/User/User';

export default function Dashboard() {

    return (
        <SidebarContainer>
            <Switch>
                <Route path="/" exact component={() => <Redirect to='/sessions' />} />
                <Route path="/sessions" component={Sessions} />
                <Route path="/user" component={User} />
            </Switch>
        </SidebarContainer>
    )

}
