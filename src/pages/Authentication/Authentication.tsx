import React from 'react'
import { Switch, Route, useRouteMatch } from 'react-router-dom'

import AuthContainer from '../../components/UI/AuthContainer/AuthContainer';

import Signin from '../../components/Authentication/Signin/Signin';
import Signup from '../../components/Authentication/Signup/Signup';
import RecoverPassword from '../../components/Authentication/RecoverPassword/RecoverPassword';

export default function Authentication() {

    let { path } = useRouteMatch();

    return (
        <AuthContainer>
            <Switch>
                <Route exact path={path} component={Signin} />
                <Route path={`${path}/signup`} component={Signup} />
                <Route path={`${path}/recover`} component={RecoverPassword} />
            </Switch>
        </AuthContainer>
    )

}