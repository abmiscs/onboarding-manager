
export interface TimestampModel {
    createdAt: number;
    updatedAt: number;
}

export interface SessionModel {
    sessionId: string;
}

// ES INDEX
export interface Session extends TimestampModel, SessionModel {
    modules: Module[];
    identity: string;
    confirmed: boolean;
}

export interface Module extends TimestampModel {
    id: string;
    type: ModuleType;
    title: string;
    completed: boolean;
    order: number;
}

export enum ModuleType {
    DOCUMENT_UPLOAD = "DOCUMENT_UPLOAD",
    IDENTITY_DATA = "IDENTITY_DATA",
    OTP_EMAIL = "OTP_EMAIL",
    OTP_SMS = "OTP_SMS",
    SELFIE_MATCH = "SELFIE_MATCH",
    SCIPAFI_VERIFY = "SCIPAFI_VERIFY",
    LIVENESS_CHECK = "LIVENESS_CHECK",
}

// ES INDEX
export interface OnboardingSession extends TimestampModel, SessionModel {
    ocr: Ocr;
    identity: Identity;
    biometrics: Biometrics;
    liveness: Liveness;
}

export interface Biometrics extends TimestampModel {
    verified: boolean;
    confidence: number;
}

export interface Liveness extends TimestampModel {
    verified: boolean;
    confidence: number;
}

export interface Identity extends TimestampModel {
    completed: boolean;
    name: string;
    lastname: string;
    fiscalCode: string;
    gender: string;
    address: string;
    city: string;
    province: string;
    country: string;
    dob: string;
    birthCity: string;
    bithProvince: string;
    birthCountry: string;
    email: string;
    emailVerified: boolean;
    phone: string;
    phoneVerified: boolean;
    addressLine: string;
}

export interface Ocr extends TimestampModel {
    completed: boolean;
    documentType: string;
    documentNumber: string;
    documentIssueDate: string;
    documentExpiryDate: string;
    name: string;
    lastname: string;
    address: string;
    city: string;
    country: string;
    dob: string;
    birthCity: string;
    gender: string;
}

// ES INDEX
export interface Selfies extends TimestampModel, SessionModel {
    base64: string[];
    detectedFace: string;
    confidence: number;
    used: boolean;
}

// ES INDEX
export interface Document extends TimestampModel, SessionModel {
    id: string;
    base64: string;
    documentType: string;
    nationality: string;
    guessedType: string;
    confirmed: boolean;
}


export interface Paging {
    page?: number;
    size?: number;
}

export interface Sorting {
    field: string;
    order?: boolean;
    raw?: boolean;
}