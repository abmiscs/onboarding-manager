export interface ErrorModel {
    message?: string;
    code?: string;
    error?: any;
}

export interface ApiResponse<T> {
    message?: string,
    results?: T,
    error?: ErrorModel
}