
export const SESSIONS_SERVICE_API = process.env.REACT_APP_SESSIONS_SERVICE_API;
export const LIVENESS_SERVICE_API = process.env.REACT_APP_LIVENESS_SERVICE_API;
export const DOCUMENTS_SERVICE_API = process.env.REACT_APP_DOCUMENTS_SERVICE_API;
export const OTP_SERVICE_API = process.env.REACT_APP_OTP_SERVICE_API;
export const OCR_SERVICE_API = process.env.REACT_APP_OCR_SERVICE_API;
export const DATA_SERVICE_API = process.env.REACT_APP_DATA_SERVICE_API;
export const DEAUTH_SERVICE_API = process.env.REACT_APP_DEAUTH_SERVICE_API;

export const API = {
    configService: {
        getSessions: `${SESSIONS_SERVICE_API}/`,
        create: `${SESSIONS_SERVICE_API}/`,
        getOnboarding: `${SESSIONS_SERVICE_API}/onboarding`,
        confirmSession: `${SESSIONS_SERVICE_API}/confirmed`,
        deleteSession: `${SESSIONS_SERVICE_API}`,
    },
    livenessService: {
        init: `${LIVENESS_SERVICE_API}/init`,
        verify: `${LIVENESS_SERVICE_API}/verify`,
        getSelfies: `${LIVENESS_SERVICE_API}/frames`,
        getSelfiesCriteria: `${LIVENESS_SERVICE_API}/frames`,
        getCurrentSelfies: `${LIVENESS_SERVICE_API}/frames/current`,
    },
    identityService: {
        confirmData: `${DATA_SERVICE_API}/data/confirm`,
        updateData: `${DATA_SERVICE_API}/data/update`,
        authenticate: `${DATA_SERVICE_API}/authenticate`,
    },
    documentsService: {
        upload: `${DOCUMENTS_SERVICE_API}/`,
        uploadBlob: `${DOCUMENTS_SERVICE_API}/uploadblob`,
        matchFrames: `${DOCUMENTS_SERVICE_API}/matchframes`,
        confirm: `${DOCUMENTS_SERVICE_API}/confirmDocument`,
        checkDocuments: `${DOCUMENTS_SERVICE_API}/checkConfirmedDocument`,
        getDocuments: `${DOCUMENTS_SERVICE_API}`,
        deleteDocuments: `${DOCUMENTS_SERVICE_API}`,
    },
    otpService: {
        request: `${OTP_SERVICE_API}/`,
        verify: `${OTP_SERVICE_API}/verify`
    },
    ocrService: {
        extractData: `${OCR_SERVICE_API}/`,
    },
    deauthService: {
        authenticate: `${DEAUTH_SERVICE_API}/authenticate`,
    }
}

// LIVENESS
export const DIRECTION_LEFT = "Left";
export const DIRECTION_RIGHT = "Right";
export const DIRECTION_CENTER = "Center";
export const MOVE_INIT = "MOVE_INIT";
export const LIVENESS_SUCCESS = "SUCCESS";
export const LIVENESS_ERROR = "ERROR";

export const BITRATES = {
    gb1: 8000000000,
    mb100: 800000000,
    mb1: 8000000,
    kb100: 800000,
    kb1: 8000,
    b100: 800
}
