import React from 'react'
import { NavLink } from 'react-router-dom'

import { FaIdCard, FaChartBar, FaPowerOff } from 'react-icons/fa'

import styled from 'styled-components'
import { useAuthDispatch } from '../../../context/AuthProvider'

const StyledDiv = styled.div`
    background-color: #003057;
    height: calc(100vh - 8vh);
    padding: 0;
    position:fixed;
    top:8vh;
    bottom:0;
    left:0;
    @media only screen and (min-width: 981px) {
        position:initial;
    }
    svg {
        color: #fff;
        margin: 50px 25px;
        font-size: 1.5em;
        display: block;
    }

    svg:hover {
        color: #71CC98;
    }
`

function Sidebar() {

    const dispatch = useAuthDispatch();

    return (
        <StyledDiv className="col-2 col-md-auto">
            <NavLink activeClassName="active" to="/sessions"><FaIdCard></FaIdCard></NavLink>
            <FaChartBar></FaChartBar>
            <FaPowerOff onClick={() => dispatch({ type: 'logout' })} style={{ cursor: 'pointer' }}></FaPowerOff>
        </StyledDiv>

    )
}

export default Sidebar