import React from 'react'

export default function AuthContainer({ children }: any) {

    return (
        <section className="container-fluid">
            <div className="row h-100vh justify-content-center align-items-center">
                {children}
            </div>
        </section>
    )

}