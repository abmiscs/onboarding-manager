import styled, { StyledComponent } from "styled-components";

interface StyledTextProps {
    color?: string,
    size?: number,
    display?: string,
    weight?: number,
    border?: boolean,
    padding?: number,
    cursor?: string
}

const StyledText: StyledComponent<"span", any, StyledTextProps, never> = styled.span`
    font-size:  ${(props: any) => props.size + 'em' || 'inherit'};
    font-weight:  ${(props: any) => props.weight || 'inherit'};
    color: ${(props: any) => props.color || 'inherit'};
    display: ${(props: any) => props.display || 'inline'};
    padding: ${(props: any) => props.padding + 'px' || '0'};
    border: ${(props: any) => props.border ? `1px solid ${props.color || '#000'}` : 'none'}
    box-sizing: border-box;
    cursor: ${(props: any) => props.cursor ? props.cursor : 'inherit'}
`

export default StyledText;