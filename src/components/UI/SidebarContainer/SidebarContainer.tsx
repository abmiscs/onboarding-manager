import React from 'react';
import AuthenticatedNavbar from '../AuthenticatedNavbar/AuthenticatedNavbar';
import Sidebar from '../Sidebar/Sidebar';
import styled from 'styled-components'

const StyledDiv = styled.div`
    margin-left:16%;
    @media only screen and (min-width: 981px) {
        margin-left:0;
    }
`

function SidebarContainer(props: any) {

    return (
        <>
            <AuthenticatedNavbar />
            <section className={["container-fluid h-container", props.classes ? props.classes : null].join(" ")}>
                <div className="row justify-content-center">
                    <Sidebar />
                    <StyledDiv className={["col-10 col-md p-5"].join(" ")} id="main">
                        {props.children}
                    </StyledDiv>
                </div>
            </section>
        </>
    )

}

export default SidebarContainer
