import React from 'react';
import styled from 'styled-components';
import { Spinner } from 'react-bootstrap';

import intesa_spinner from '../../../assets/img/intesa_spinner.svg'

const StyledSpinner = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
`

function LoadingSpinner(props: any) {
    return (
        <StyledSpinner>
            <img src={intesa_spinner} width="80" alt="logo" />
            <span className="mt-3">{props.message}</span>
        </StyledSpinner>
    )
}

export default LoadingSpinner;

        // <Spinner animation="border" role="status">
        //     <span className="sr-only">{props.message}</span>
        // </Spinner>