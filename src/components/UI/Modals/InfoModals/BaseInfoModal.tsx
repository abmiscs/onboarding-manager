import React from 'react'

import { Button, Modal } from 'react-bootstrap';

interface BaseInfoModalProps {
    show: boolean,
    children: React.ReactNode,
    btnText?: string,
    onHide?: () => void
}

export default function BaseInfoModal({ show, children, btnText, onHide }: BaseInfoModalProps) {

    const handleHide = () => {
        if (onHide) onHide();
    }

    return (
        <Modal show={show} onHide={handleHide} centered>
            <Modal.Body>
                {children}
            </Modal.Body>
            <Modal.Footer className="px-5 text-center">
                <Button className="w-100" variant="primary" onClick={handleHide}>{btnText || 'Ok'}</Button>
            </Modal.Footer>
        </Modal>
    )
}