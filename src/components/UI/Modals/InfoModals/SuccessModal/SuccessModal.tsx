import React from 'react'

import BaseInfoModal from '../BaseInfoModal'

import success_v from '../../../../../assets/img/success_v.svg'

interface SuccessModalProps {
    show: boolean,
    text: string,
    onHide?: () => void
}

export default function SuccessModal({ show, text, onHide }: SuccessModalProps) {

    return (
        <BaseInfoModal show={show} onHide={onHide} btnText="Ok">
            <h2 className="px-5">MATCH CONFERMATO</h2>
            <div className="row justify-content-center">
                <div className="col-auto p-5">
                    <img src={success_v} className="d-block" />
                </div>
            </div>
        </BaseInfoModal>
    )

}

