import React from 'react'

import BaseInfoModal from '../BaseInfoModal'

import error_icon from '../../../../../assets/img/error_x.svg'

interface ErrorModalProps {
    show: boolean,
    text: string,
    onHide?: () => void
}

export default function ErrorModal({ show, text, onHide }: ErrorModalProps) {

    return (
        <BaseInfoModal show={show} onHide={onHide} btnText="Ok">
            <h2 className="px-5">{text}</h2>
            <div className="row justify-content-center">
                <div className="col-auto p-5">
                    <img src={error_icon} className="d-block" />
                </div>
            </div>
        </BaseInfoModal>
    )

}
