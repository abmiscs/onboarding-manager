import React from 'react'

import { Button, Modal } from 'react-bootstrap'

interface ConfirmModalProps {
    show: boolean,
    children: React.ReactNode,
    confirmText?: string,
    cancelText?: string,
    action?: string,
    onConfirm: (action?: string) => void,
    onCancel: (action?: string) => void,
    onHide?: () => void
}

export default function ConfirmModal({ show, children, confirmText, cancelText, action, onConfirm, onCancel, onHide }: ConfirmModalProps) {

    const handleHide = () => {
        if (onHide) onHide();
    }

    return (
        <Modal show={show} onHide={handleHide} centered>
            <Modal.Header closeButton></Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            <Modal.Footer className="px-5">
                <Button variant="secondary" onClick={() => onCancel(action)}>{cancelText || 'Annulla'}</Button>
                <Button variant="primary" onClick={() => onConfirm(action)} className="ml-auto">{confirmText || 'Conferma'}</Button>
            </Modal.Footer>
        </Modal>
    )

}