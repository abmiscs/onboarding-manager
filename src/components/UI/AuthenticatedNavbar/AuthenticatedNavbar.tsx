import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import logo_placeholder from '../../../assets/img/logo_placeholder.svg'
import upload_avatar from '../../../assets/img/upload_avatar.svg'

const StyledNav = styled.nav`
    border: none;
    box-shadow: none;
    background-color: #DEF0FA !important;
    position:fixed;
    left:0;
    right:0;
    z-index:10;

    height: 8vh;

    .navbar_logo{
        height: 100%;
        display: inline-block;
    }
    .user_options{
        span{
            display:inline-flex;
            align-items:center;
            height: 100%;
            justify-content: center;
        }
        .navbar_avatar{
            border-radius: 50%;
            height:60%;
            width:auto;
            object-fit: cover;
            overflow: hidden;
            display: inline-block;
            border:1px solid #003057;
            margin: 0 10px;
        }
    }
`

function AuthenticatedNavbar(props: any) {
    return (
        <StyledNav className="navbar row p-0 align-items-center h_nav" >
            <div className="col h-100">
                <Link className="navbar-brand d-block p-0 h-100 d-block" to="/">
                    <img src={logo_placeholder} className={["align-top", "navbar_logo"].join(" ")} />
                </Link>
            </div>
            <div className="col h-100 text-right user_options">
                <span className="m-0 font-weight-bold">Buongiorno Mario Rossi</span>
                <Link to="/user/profile"><img src={upload_avatar} className={["navbar_avatar"].join(" ")} /></Link>
            </div>
        </StyledNav>
    )
}

export default AuthenticatedNavbar;