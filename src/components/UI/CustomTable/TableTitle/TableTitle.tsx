import styled from "styled-components";

const TableTitle = styled.h1`
    text-align: center;
    color: #fff;
    font-size: 1.8em;
    background-color: #003057;
    font-weight: 100;
    padding: 15px;
    margin-bottom: 0;
`
export default TableTitle