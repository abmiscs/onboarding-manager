import React from 'react'
import styled from 'styled-components'

const StyledTable = styled.table`
    max-height: 65vh;
    @media only screen and (min-width: 981px) {
      max-height: 69vh;
    }
    table-layout: fixed;
    overflow-y: auto;
    display: block;
    margin-bottom: 0 !important;
    thead{
      tr{
        @media only screen and (min-width: 981px) {
        display: table;
        width: 100%;
        table-layout: fixed;
        background-color: #DEF0FA;
        }
        cursor: pointer;
        th{
          padding: 0.5rem;
          color: #003057;
          border:1px solid #003057 !important;
        }
      }
    }
    tbody{
      tr{
        @media only screen and (min-width: 981px) {
        display: table;
        width: 100%;
        table-layout: fixed;
        }
        cursor: pointer;
        td{
          padding: 0.5rem;
          color: #003057;
          border:1px solid #dee2e6;
          vertical-align:middle;
          img{
              max-height:25px;
          }
        }
      }
      tr:nth-child(even){
        background-color:#F7F7F9;
      }
      tr:hover{
        background-color: #EDEDF1;
      }
    }
`

export default function CustomTable({ children, classes }: any) {
    return (
        <StyledTable className={`table ${classes ? classes : ''}`}>
            {children}
        </StyledTable>
    )
}