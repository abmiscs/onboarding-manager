import React from 'react'
import styled from 'styled-components'

const StyledDiv = styled.div`
    overflow-y: auto;
    margin-bottom:15px;
    @media only screen and (min-width: 981px) {
        max-height: 83%;
    }
    @media only screen and (min-width: 1500px) {
        max-height: 88%;
    }

    .card-header{
        background-color:#003057;
        border:1px solid #003057;
        color:#fff;
        text-align:center;
        padding:0.7em;
        h5{
            font-size:1.4em;
            font-weight:100;
            margin:0;
        }
    }
    >img{
        border-radius: 50%;
        height: 100px;
        width: 100px;
        object-fit: cover;
        overflow: hidden;
        border: 1px solid #003057;
        margin: 30px auto;
    }
    .card-body{
        padding:1em 3em;
        label, p{
            border-bottom:1px solid #A9A9A9;;
            margin:0;
            padding:0;
            font-weight:600;
        }
        input, p{
            border: 1px solid #A9A9A9;
            line-height:2em;
            min-height:2em;
            padding:0 10px;
            font-weight:100;
        }
    }
`
export default function CardTable({ header, children, classes }: any) {
    return (
        <StyledDiv className={`card card-table ${classes ? classes : ''}`}>
            {header && <div className="card-header">{header}</div>}
            <div className="card-body">{children}</div>
        </StyledDiv>

    )
}
