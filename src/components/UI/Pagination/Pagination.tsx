import React from 'react';

import styled from 'styled-components';

const StyledUl = styled.ul`
    justify-content: center;
    .page-link {
        cursor: pointer;
    }
`

interface PaginationProps {
    next: any,
    previous: any,
    pageChange: (index: number) => void,
    size: number,
    total: number,
    current: number
}

function Pagination(props: PaginationProps) {

    let pages = null;

    if (props.total && props.total > 0 && props.size && props.size > 0) {
        pages = Array.from(
            new Array(Math.round(props.total / props.size)),
            (el, index) => (
                <li className={`page-item ${index === (props.current - 1) ? 'active' : ''}`} key={index}><span className="page-link" onClick={() => props.pageChange(index + 1)}>{index + 1}</span></li>
            ),
        )
    }

    let component = null;
    if (pages) {
        component = <nav aria-label="Page navigation example">
            <StyledUl className="pagination">
                <li className="page-item">
                    <span className="page-link" aria-label="Previous" onClick={props.previous} style={{ cursor: 'pointer' }}>
                        <span aria-hidden="true">&laquo;</span>
                        <span className="sr-only">Previous</span>
                    </span>
                </li>
                {pages}
                <li className="page-item">
                    <span className="page-link" aria-label="Next" onClick={props.next} style={{ cursor: 'pointer' }}>
                        <span aria-hidden="true">&raquo;</span>
                        <span className="sr-only">Next</span>
                    </span>
                </li>
            </StyledUl>
        </nav>
    }

    return component

}

export default Pagination;