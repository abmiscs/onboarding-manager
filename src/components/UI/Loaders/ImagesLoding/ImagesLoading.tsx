import React from "react"

import StyledLoader from "../StyledLoading"

import { ReactComponent as ImagesLoadingSvg } from '../../../../assets/img/placeholders/images_loading_placeholder.svg'

const ImagesLoading = () => (
    <StyledLoader>
        <ImagesLoadingSvg />
    </StyledLoader>
)

export default ImagesLoading