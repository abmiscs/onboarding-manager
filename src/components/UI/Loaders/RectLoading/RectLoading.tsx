import React from "react"

import StyledLoader from "../StyledLoading"

import { ReactComponent as RectLoadingSvg } from '../../../../assets/img/placeholders/rect_loading_placeholder.svg'

const RectLoading = () => (
    <StyledLoader>
        <RectLoadingSvg />
    </StyledLoader>
)

export default RectLoading