import React from "react"

import StyledLoader from "../StyledLoading"

import { ReactComponent as DataLoading } from '../../../../assets/img/placeholders/datalist_loading_placeholder.svg'

const DataListLoading = () => (
    <StyledLoader>
        <DataLoading />
    </StyledLoader>
)

export default DataListLoading