import styled from "styled-components"

const StyledLoader = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    justify-items: center;
    align-content: center;
    align-items: center;

    svg{
        flex: 1;
        display: flex;
        justify-content: center;
        justify-items: center;
        align-content: center;
        align-items: center;
    }
`

export default StyledLoader