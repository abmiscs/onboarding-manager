import React from "react"

import StyledLoader from "../StyledLoading"

import { ReactComponent as SquareLoadingSvg } from '../../../../assets/img/placeholders/square_loading_placeholder.svg'

const SquareLoading = () => (
    <StyledLoader>
        <SquareLoadingSvg />
    </StyledLoader>
)

export default SquareLoading