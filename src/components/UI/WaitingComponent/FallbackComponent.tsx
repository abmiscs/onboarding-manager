import React from 'react'
import styled from 'styled-components'

import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';

const StyledDiv = styled.div`
    width: 100%;
    height: 100%;
    dispaly: flex;
    justify-content: center;
    align-content: center;
    align-items: center;
`

function FallbackComponent() {
    return (
        <StyledDiv>
            <LoadingSpinner />
        </StyledDiv>
    )
}

export default FallbackComponent