import React from 'react'

import logo_intesa from '../../../assets/img/intesa_logo_affiliate_col.svg'

export default function LogoRow() {
    return (
        <div className="row justify-content-center mb-5">
            <div className="col-5">
                <img src={logo_intesa} className="d-inline-block align-top navbar-logo" alt="" />
            </div>
        </div>
    )
}