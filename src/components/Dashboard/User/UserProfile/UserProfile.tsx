import React from 'react'

import CardTable from '../../../UI/CardTable/CardTable'

import upload_avatar from '../../../../assets/img/upload_avatar.svg'

function UserProfile() {

    return (
        <div className="row h-100 justify-content-center align-items-center">
            <div className="col col-md-4">
                <CardTable header={<h5>Modifica profilo personale</h5>}>
                    <img src={upload_avatar} className="d-block" />
                        <div className="form-group row">
                            <label className="col">Username</label>
                            <input className="col" />
                        </div>
                        <div className="form-group row">
                            <label className="col">Nuova password</label>
                            <input className="col" />
                        </div>
                        <div className="form-group row">
                            <label className="col">Ripeti password</label>
                            <input className="col" />
                        </div>
                        <div className="form-group row">
                            <label className="col">Email</label>
                            <input className="col" />
                        </div>
                        <div className="form-group row mt-5">
                            <div className="col">
                                <button type="submit" className="w-100 btn btn-secondary">Annulla</button>
                            </div>
                            <div className="col">
                                <button type="submit" className="w-100 btn btn-primary">Salva</button>
                            </div>
                        </div>
                </CardTable>
            </div>
        </div>
    )

}

export default UserProfile;