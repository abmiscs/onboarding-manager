import React from 'react'
import { Route, Redirect, useRouteMatch, Switch } from 'react-router-dom'

import UserProfile from './UserProfile/UserProfile'

export default function User() {

    const { path } = useRouteMatch();

    return (
        <Switch>
            <Route path={`${path}/profile`} component={UserProfile} />
            <Route path={`/`} component={() => <Redirect to={`${path}/profile`} />} />
        </Switch>
    )

}
