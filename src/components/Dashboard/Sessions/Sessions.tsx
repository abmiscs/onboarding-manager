import React from 'react'
import { Route, useRouteMatch } from 'react-router-dom'

import SessionsTable from './SessionsTable/SessionsTable'
import SessionsDetails from './SessionDetails/SessionDetails'

export default function Sessions() {

    const { path } = useRouteMatch();

    return (
        <>

            <Route path={`${path}/:sessionId`} component={SessionsDetails} />
            <Route path={`${path}/`} exact component={SessionsTable} />
        </>
    )

}