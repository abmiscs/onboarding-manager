import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

import Moment from 'react-moment';

import { Session, Module, ModuleType } from '../../../../../models/Models'

import green from '../../../../../assets/img/green.svg'
import red from '../../../../../assets/img/red.svg'
import yellow from '../../../../../assets/img/yellow.svg'

interface SessioRowProps {
    session: Session;
}

export default function SessionRow({ session }: SessioRowProps) {

    const history = useHistory()

    const [currentModule, setCurrentModule] = useState<number>(0);

    useEffect(() => {
        let index = 0
        session.modules.map((module: Module, idx: number) => {
            if (module.completed) {
                index = idx;
            }
        });
        setCurrentModule(index);
    })

    const getCurrentModule = () => {
        return (<span>{`${currentModule + 1} - ${session.modules[currentModule]?.title}`}</span>)
    }

    const getMatch = () => {
        if (currentModule !== session.modules.length - 1) return <img src={yellow} className="d-block m-auto" />
        const selfie = session?.modules?.find((module: Module) => module.type === ModuleType.SELFIE_MATCH)?.completed;
        if (selfie === true) return <img src={green} className="d-block m-auto" />;
        if (selfie === false) return <img src={red} className="d-block m-auto" />;
        return <img src={yellow} className="d-block m-auto" />;
    }

    return (
        <tr onClick={() => history.push(`/sessions/${session.sessionId}`)}>
            <td scope="col">{session.identity || 'N.A.'}</td>
            <td scope="col">{session.updatedAt ? <Moment date={new Date(session.updatedAt)} format="DD/MM/YY HH:mm:ss" /> : 'N.A'}</td>
            <td scope="col">{getCurrentModule()}</td>
            <td scope="col">{getMatch()}</td>
        </tr>
    )

}