import React, { useState, useEffect } from 'react'

import { usePaging } from '../../../../hooks/usePaging';
import { useLoading } from '../../../../hooks/useLoading';

import SessionService from '../../../../services/SessionService/SessionService';

import { Session, Sorting } from '../../../../models/Models';

import Pagination from '../../../UI/Pagination/Pagination';
import CustomTable from '../../../UI/CustomTable/CustomTable';
import TableTitle from '../../../UI/CustomTable/TableTitle/TableTitle';

import SessionRow from './SessionRow/SessionRow';
import Searchbar from './Searchbar/Searchbar';

import TableLoading from '../../../UI/Loaders/TableLoading/TableLoading';

export default function SessionsTable() {

    const [{ page, size, total }, setValue, prevPage, nextPage] = usePaging();
    const [{ loading, loadingMsg, loadingError }, toggleLoading, setLoadingError] = useLoading();

    const [firstLoad, setFirstLoad] = useState(true);

    const [sessions, setSessions] = useState<Session[]>([]);

    useEffect(() => {
        getSessions();
    }, [page, size])

    const getSessions = async () => {
        try {
            toggleLoading()
            const sorting: Sorting = { field: 'updatedAt' };

            const response = await SessionService.getSessions({ page, size }, sorting);
            if (response.results) {
                setSessions(response.results.results);
                setValue('total', response.results.total);
            }

            toggleLoading()
        } catch (error) {
            console.log(error);
            setLoadingError('Could not load sessions');
        }
        if (firstLoad) setFirstLoad(false)
    }

    const rows = sessions?.map((session: Session, idx: number) => <SessionRow session={session} key={idx} />)

    return (
        <>
            {!firstLoad && <>
                <Searchbar />
                <TableTitle>CLIENTI</TableTitle>
                <CustomTable classes="text-center">
                    <thead>
                        <tr>
                            <th scope="col">Identità</th>
                            <th scope="col">Data di compilazione</th>
                            <th scope="col">Step completati</th>
                            <th scope="col">Match</th>
                        </tr>
                    </thead>
                    <tbody style={{ opacity: loading ? 0.5 : 1 }}>
                        {rows || <tr><td colSpan={4}>Nessuna sessione trovata</td></tr>}
                    </tbody>
                </CustomTable>
                <div className="row mt-3">
                    <div className="col">
                        {total > size && <Pagination
                            size={size}
                            current={page}
                            total={total}
                            pageChange={(index) => setValue('page', index)}
                            previous={prevPage}
                            next={nextPage}
                        />}
                    </div>
                </div>
            </>}
            {firstLoad && <TableLoading />}
        </>
    )

}

