import React from 'react'
import styled from 'styled-components'

import { FaSearch } from 'react-icons/fa';

const StyledDiv = styled.div`
    svg{
        top:10px;
        left:10px;
    }
    input{
        padding-left: 35px;
    }
`

interface SearchbarProps {
    onChange?: (value: string) => void,
    onBlur?: (value: string) => void
}

export default function Searchbar({ onChange, onBlur }: SearchbarProps) {

    const onValueChange = (value: string) => {
        if (onBlur) return onBlur(value);
        if (onChange) onChange(value);
    }

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => onValueChange(e.target.value)
    const onBlurHandler = (e: React.ChangeEvent<HTMLInputElement>) => onValueChange(e.target.value)

    return (
        <StyledDiv className="form-group position-relative">
            <FaSearch className="position-absolute" />
            <input
                type="text"
                name="search"
                className="form-control"
                id="searchInput"
                placeholder="Ricerca rapida"
                onChange={onChangeHandler}
                onBlur={onBlurHandler}
            />
        </StyledDiv>
    )

}