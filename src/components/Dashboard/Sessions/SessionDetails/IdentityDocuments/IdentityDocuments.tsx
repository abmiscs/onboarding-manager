import React, { useState, useEffect } from 'react'

import classes from './IdentityDocuments.module.scss'

import { Document, Selfies } from '../../../../../models/Models'

import { useModals } from '../../../../../hooks/useModals'
import { useLoading } from '../../../../../hooks/useLoading'

import DocumentService from '../../../../../services/DocumentService/DocumentService'
import SelfiesService from '../../../../../services/SelfiesServce/SelfiesService'

import PreviewModal from './PreviewModal/PreviewModal'
import ComparePictures from '../ComparePictures/ComparePictures'

import CardTable from '../../../../UI/CardTable/CardTable'
import ConfirmModal from '../../../../UI/Modals/ConfirmModal/ConfirmModal'
import SuccessModal from '../../../../UI/Modals/InfoModals/SuccessModal/SuccessModal'
import ErrorModal from '../../../../UI/Modals/InfoModals/ErrorModal/ErrorModal'
import ImagesLoading from '../../../../UI/Loaders/ImagesLoding/ImagesLoading'

import upload_avatar from '../../../../../assets/img/upload_avatar.svg'
import placeholder from '../../../../../assets/img/image-placeholder.png'
import { DocumentsRow } from '../DocumentsRow/DocumentsRow'

interface IdentityDocumentsProps {
    sessionId: string
}

export default function IdentityDocuments({ sessionId }: IdentityDocumentsProps) {

    const [docsLoading, toggleDocLoading, setDocLoadingError] = useLoading();
    const [selfiesLoading, toggleSelfiesLoading, setSelfiesLoadingError] = useLoading();

    const [modals, openModal, closeModal] = useModals({ preview: false, confirm: false, deny: false, ok: false, ko: false })

    const [showCompare, setShowCompare] = useState(false);

    const [previewPicture, setPreviewPicture] = useState<string>();
    const [toCompare, setToCompare] = useState<string[]>([]);

    const [documents, setDocuments] = useState<Document[]>();
    const [currentSelfie, setCurrentSelfie] = useState<Selfies>();

    useEffect(() => {
        getDocuments();
        getCurrentSelfies();
    }, [])

    const getDocuments = async () => {
        try {
            toggleDocLoading()
            const response = await DocumentService.getDocuments(sessionId)
            if (!response) {
                throw Error('No response')
            }
            setDocuments(response.results)
            toggleDocLoading()
        } catch (error) {
            console.log(error)
            setDocLoadingError('Non è stato possibile recuperare i documenti');
        }
    }

    const getCurrentSelfies = async () => {
        try {
            toggleSelfiesLoading()
            const response = await SelfiesService.getCurrentSelfies(sessionId)
            if (!response) {
                throw Error('No response')
            }
            if (response.results?.results?.length == 1) {
                setCurrentSelfie(response.results?.results[0])
            }
            toggleSelfiesLoading()
        } catch (error) {
            console.log(error)
            setSelfiesLoadingError('Non è stato possibile recuperare i selfies');
        }
    }

    const handlePreview = (img: string) => {
        console.log('handlePreview')
        setPreviewPicture(img)
        openModal('preview')
    }

    const handleSelect = () => {
        if (!previewPicture) return;
        let comparePictures = toCompare;
        comparePictures.push(previewPicture)
        setToCompare(comparePictures)
        closeModal('preview')
        setPreviewPicture('')
    }

    const handleDeselect = (base64: string) => {
        let comparePictures: any = [];
        if (toCompare.length > 0) {
            comparePictures = toCompare.filter((pic: string) => pic !== base64)
            setToCompare(comparePictures)
        }
    }

    const handleHidePreview = () => {
        setPreviewPicture('')
        closeModal('preview')
    }

    const handleConfirm = () => {
        closeModal('confirm');
        openModal('ok')
    }

    const handleDeny = () => {
        closeModal('deny');
        openModal('ko')
    }

    const actions =
        <div className="row mt-5">
            {!showCompare && <div className="col-12 col-md px-5">
                <button onClick={() => setShowCompare(true)} className="w-100 btn btn-primary mb-1" disabled={toCompare?.length < 2}>Confronta</button>
            </div>}
            {showCompare && <div className="col-12 col-md px-5">
                <button onClick={() => setShowCompare(false)} className="w-100 btn btn-primary mb-1">Indietro</button>
            </div>}
            <div className="col-12 col-md px-5 text-center">
                <button onClick={() => openModal('confirm')} className="btn w-100 btn-success mb-1">Match ok</button>
            </div>
            <div className="col-12 col-md px-5 text-right">
                <button onClick={() => openModal('deny')} className="btn w-100 btn-danger mb-1">No match</button>
            </div>
        </div>

    let documentsComponent = null;

    if (!showCompare) {
        const documentsPics = documents?.map((doc: Document, idx: number) => {
            const selected = doc.base64 === toCompare?.find((pic: string) => pic === doc.base64);
            return (
                <div className={`col-12 col-md-auto ${selected ? classes.Selected : ''}`} key={idx}>
                    {selected && <div className={classes.Tick} />}
                    <img onClick={selected ? () => handleDeselect(doc.base64) : () => handlePreview(doc.base64)} src={`data:image/jpg;base64,${doc.base64}`} className="d-block" />
                </div>
            )
        })
        const selfiesPics = currentSelfie?.base64?.map((b64: string, idx: number) => {
            const selected = b64 === toCompare?.find((pic: string) => pic === b64);
            return (
                <div className={`col-12 col-md-auto ${selected ? classes.Selected : ''}`} key={idx}>
                    {selected && <div className={classes.Tick} />}
                    <img onClick={selected ? () => handleDeselect(b64) : () => handlePreview(b64)} src={`data:image/jpg;base64,${b64}`} className="d-block" />
                </div>
            )
        })

        documentsComponent =
            <>

                <DocumentsRow
                    documents={documentsPics}
                    loading={docsLoading.loading}
                    loadingError={docsLoading.loadingError}
                />
                <DocumentsRow
                    documents={selfiesPics}
                    loading={selfiesLoading.loading}
                    loadingError={selfiesLoading.loadingError}
                />
            </>
    }

    const compareComponent = showCompare && <ComparePictures pictures={toCompare} />

    return (
        <>
            <CardTable header={<h5>Documenti Caricati</h5>}>
                {documentsComponent}
                {compareComponent}
                {actions}
            </CardTable>
            <PreviewModal show={modals.preview} imgSrc={previewPicture ? `data:image/jpg;base64,${previewPicture}` : ''} onSelect={handleSelect} onHide={handleHidePreview}></PreviewModal>
            <ConfirmModal show={modals.confirm} onConfirm={handleConfirm} onCancel={() => closeModal('confirm')} onHide={() => closeModal('confirm')}><h2 className="text-center p-5">Vuoi confermare il match?</h2></ConfirmModal>
            <ConfirmModal show={modals.deny} onConfirm={handleDeny} onCancel={() => closeModal('deny')} onHide={() => closeModal('deny')} ><h2 className="text-center m-5">Vuoi negare il match?</h2></ConfirmModal>
            <SuccessModal show={modals.ok} onHide={() => closeModal('ok')} text="MATCH CONFERMATO" />
            <ErrorModal show={modals.ko} onHide={() => closeModal('ko')} text="MATCH NON CONFERMATO" />
        </>

    )

}