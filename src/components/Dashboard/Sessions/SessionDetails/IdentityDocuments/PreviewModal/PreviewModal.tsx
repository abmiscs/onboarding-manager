import React from 'react'

import { Button, Modal } from 'react-bootstrap';

interface PreviewModalProps {
    show: boolean,
    imgSrc: string,
    onSelect: () => void,
    onHide?: () => void
}

export default function PreviewModal({ show, imgSrc, onSelect, onHide }: PreviewModalProps) {

    const handleHide = () => {
        if (onHide) onHide();
    }

    return (
        <Modal show={show} onHide={handleHide} centered>
            <Modal.Header closeButton></Modal.Header>
            <Modal.Body>
                <img src={imgSrc} className="d-block" />
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" className="mx-auto" onClick={onSelect}>
                    seleziona per il confronto
                </Button>
            </Modal.Footer>
        </Modal>
    )

}
