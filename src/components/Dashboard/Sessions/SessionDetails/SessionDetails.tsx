import React, { useState, useEffect } from 'react'
import { useParams, Switch, Route, useRouteMatch } from 'react-router-dom';

import { OnboardingSession } from '../../../../models/Models';

import { useLoading } from '../../../../hooks/useLoading';

import SessionService from '../../../../services/SessionService/SessionService';

import IdentityData from './IdentityData/IdentityData';
import IdentityDocuments from './IdentityDocuments/IdentityDocuments';

function SessionsDetails() {

    const [{ loading, loadingMsg, loadingError }, toggleLoading, setLoadingError] = useLoading();

    const [onboarding, setOnboarding] = useState<OnboardingSession>();

    let { sessionId } = useParams();

    const { path } = useRouteMatch();

    useEffect(() => {
        getOnboardinSession()
    }, [])

    const getOnboardinSession = async () => {
        try {
            if (!sessionId) return;
            toggleLoading();

            const response = await SessionService.getOnboardig(sessionId);

            if (response.results) {
                setOnboarding(response.results);
            }

            toggleLoading();
        } catch (error) {
            console.log(error);
            setLoadingError('Could not load sessions');
        }
    }

    return (
        <div className="row h-100 justify-content-center">
            <div className="col col-md-4">
                <IdentityData identity={onboarding?.identity} loading={loading} />
            </div>
            <div className="col col-md-8">
                <IdentityDocuments sessionId={sessionId || ''} />
            </div>
        </div>
    )

}

export default SessionsDetails;