import React, { ReactNode } from 'react'

import classes from './DocumentsRow.module.scss'

import ImagesLoading from '../../../../UI/Loaders/ImagesLoding/ImagesLoading'

interface DocumentsRowProps {
    documents?: ReactNode[]
    loading?: boolean,
    loadingError?: string
}

export function DocumentsRow({ documents, loading, loadingError }: DocumentsRowProps) {

    return (
        <div className={`row ${!loading ? classes.IdentityImages : ''} justify-content-center`}>
            {!loading && (documents || 'No documents uploaded')}
            {loading && <div className="col text-center"><ImagesLoading /></div>}
            {loadingError && <div className="col-auto text-danger">{loadingError}</div>}
        </div>
    )

}