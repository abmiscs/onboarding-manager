import React from 'react'
import { DocumentsRow } from '../DocumentsRow/DocumentsRow';

interface ComparePicturesProps {
    pictures: string[]
}

function ComparePictures({ pictures }: ComparePicturesProps) {

    const picturesComponents = pictures?.map((pic: string, idx: number) => <div className="col-12 col-md-6" key={idx}><img src={`data:image/jpg;base64,${pic}`} className="d-block img-fluid" /></div>)

    return (
        <DocumentsRow documents={picturesComponents} />
    )

}

export default ComparePictures;