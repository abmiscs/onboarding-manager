import React from 'react'

import { Identity, Ocr } from '../../../../../models/Models'

import CardTable from '../../../../UI/CardTable/CardTable'
import DataListLoading from '../../../../UI/Loaders/DataListLoading/DataListLoading'

interface IdentityDataProps {
    ocr?: Ocr,
    identity?: Identity,
    loading?: boolean
}

export default function IdentityData({ identity, ocr, loading }: IdentityDataProps) {

    return (
        <CardTable header={<h5>Dati Utente</h5>}>
            {!loading &&
                <>
                    <IdentityRow title='nome' value={identity?.name || ocr?.name} />
                    <IdentityRow title='cognome' value={identity?.lastname || ocr?.lastname} />
                    <IdentityRow title='sesso' value={identity?.gender || ocr?.gender} />
                    <IdentityRow title='codice fiscale' value={identity?.fiscalCode} />
                    <IdentityRow title='indirizzo' value={identity?.addressLine || ocr?.address} />
                    <IdentityRow title='città' value={identity?.city || ocr?.city} />
                    <IdentityRow title='data di nascita' value={identity?.dob || ocr?.dob} />
                    <IdentityRow title='luogo di nascita' value={identity?.birthCity || ocr?.birthCity} />
                    <IdentityRow title='email' value={identity?.email} />
                    <IdentityRow title='cellullare' value={identity?.phone} />
                    <IdentityRow title='tipo documento' value={ocr?.documentType} />
                    <IdentityRow title='numero documento' value={ocr?.documentNumber} />
                    <IdentityRow title='data di emissione' value={ocr?.documentIssueDate} />
                    <IdentityRow title='data di scadenza' value={ocr?.documentExpiryDate} />
                </>}
            {loading && <DataListLoading />}
        </CardTable>
    )

}

interface IdentityRowProps {
    title: string,
    value?: React.ReactNode
}

function IdentityRow({ title, value }: IdentityRowProps) {
    return (
        <div className="form-group row">
            <label className="col text-capitalize">{title}</label>
            <p className="col">{value || 'N.A.'}</p>
        </div>
    )
}