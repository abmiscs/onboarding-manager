import React, { useState } from 'react'
import { Link, useRouteMatch } from 'react-router-dom'

import { useForm } from 'react-hook-form'

import { FaEye, FaEyeSlash } from 'react-icons/fa'

import { useAuthDispatch } from '../../../context/AuthProvider'
import { useLoading } from '../../../hooks/useLoading'

import DeauthService from '../../../services/DeauthService/DeauthService'

import LoadingSpinner from '../../UI/LoadingSpinner/LoadingSpinner'
import StyledText from '../../UI/StyledText/StyledText'
import LogoRow from '../../UI/LogoRow/LogoRow'

import AuthCard from '../AuthCard/AuthCard'

import upload_avatar from '../../../assets/img/upload_avatar.svg'

function Signin() {

    const [pwType, setPwType] = useState(true);

    const dispatch = useAuthDispatch()
    
    const [{ loading, loadingMsg, loadingError }, toggleLoading, setLoadingError] = useLoading();

    const { handleSubmit, errors, register, formState } = useForm({
        mode: "onBlur",
        defaultValues: {
            username: '',
            password: '',
            rptpassword: ''
        }
    });

    const onSignInHandler = async (data: any) => {
        try {
            toggleLoading();
            const response = await DeauthService.login(data.username, data.password);
            if (!response.results?.authenticated) throw new Error('Registration failed')
            dispatch({
                type: 'login',
                payload: {
                    authtoken: response.results?.authtoken
                }
            })
        } catch (error) {
            setLoadingError('Non è stato possible effetuare la registrazione')
        }
    }

    return (
        <div className="col-12 col-sm-10 col-md-8 col-lg-5">
            <LogoRow />
            <form onSubmit={handleSubmit(onSignInHandler)}>
                <AuthCard>
                    <div className="card-body">
                        <div className="row">
                            <div className="col text-center">
                                <StyledText weight={100} size={1.8}>Accesso</StyledText>
                                <img src={upload_avatar} width="100" className="d-block m-auto" alt="" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <input
                                        type="text"
                                        name="username"
                                        className="form-control"
                                        id="userInput"
                                        aria-describedby="userError"
                                        placeholder="username"
                                        ref={register({ required: true })} />
                                    {errors.username && <small id="userError" className="form-text text-danger">Campo obbligatorio</small>}
                                </div>
                                <div className="form-group">
                                    <div className="input-group" id="show_hide_password">
                                        <input
                                            type={pwType ? 'password' : 'text'}
                                            name="password"
                                            className="form-control"
                                            id="passwordInput"
                                            placeholder="password"
                                            ref={register({ required: true })} />
                                        <div className="input-group-append ml-2" style={{ cursor: 'pointer' }}>
                                            <span className="form-span" onClick={() => setPwType(!pwType)}>
                                                {pwType && <FaEye />}
                                                {!pwType && <FaEyeSlash />}
                                            </span>
                                        </div>
                                    </div>
                                    {errors.password && <small id="userError" className="form-text text-danger">Campo obbligatorio</small>}
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <input type="checkbox" /> <p className="form-p d-inline-block">ricorda</p>
                                    </div>
                                    <div className="col text-right">
                                        <p className="form-p"><Link to={`/auth/recover`}>password dimenticata</Link></p>
                                    </div>
                                </div>
                                {!loading && <div className="row">
                                    <div className="col mr-5">
                                        <Link to={`/auth/signup`} className="w-100 btn btn-secondary">registrati</Link>
                                    </div>
                                    <div className="col ml-5">
                                        <button type="submit" className="w-100 btn btn-primary" disabled={!formState.isValid}>login</button>
                                    </div>
                                </div>}
                                {loading && <div className="row">
                                    <div className="col text-center"><LoadingSpinner /></div>
                                </div>}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-auto error mt-3">
                                {loadingError && <StyledText>{loadingError}</StyledText>}
                            </div>
                        </div>
                    </div>
                </AuthCard>
            </form>
        </div>
    )

}

export default Signin;