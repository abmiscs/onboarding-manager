import React from 'react'

import styled from 'styled-components'

const StyledCard = styled.div`
    background-color: #F1F1F1;
`

export default function AuthCard({ children }: any) {
    return (<StyledCard className="card">{children}</StyledCard>)
}
