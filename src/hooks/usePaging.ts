import { useReducer } from "react"

enum PagingActions {
    INIT = 'INIT',
    SET_VALUE = 'SET_VALUE',
    NEXT_PAGE = 'SET_NEXT',
    PREVIOUS_PAGE = 'PREVIOUS_PAGE'
}

type PagingAction =
    { type: PagingActions.INIT, payload: { page: number, size: number, total?: number } }
    | { type: PagingActions.SET_VALUE, payload: { key: 'page' | 'size' | 'total', value: number } }
    | { type: PagingActions.NEXT_PAGE }
    | { type: PagingActions.PREVIOUS_PAGE }


// TODO - implement and hanlde canPrev and canNext properties
type PagingState = {
    page: number,
    size: number,
    total: number
}

const initialPagingState: PagingState = {
    page: 1,
    size: 10,
    total: 0
}

const initPaging = (dispatch: React.Dispatch<PagingAction>, page: number, size: number, total?: number) => {
    return dispatch({
        type: PagingActions.INIT,
        payload: { page, size, total }
    })
}

const setValue = (dispatch: React.Dispatch<PagingAction>, key: 'page' | 'size' | 'total', value: number) => {
    return dispatch({
        type: PagingActions.SET_VALUE,
        payload: { key, value }
    })
}

const nextPage = (dispatch: React.Dispatch<PagingAction>) => {
    return dispatch({
        type: PagingActions.NEXT_PAGE
    })
}

const prevPage = (dispatch: React.Dispatch<PagingAction>) => {
    return dispatch({
        type: PagingActions.PREVIOUS_PAGE
    })
}

const pagingReducer = (state: PagingState = initialPagingState, action: PagingAction): PagingState => {
    const { page, size, total } = state;
    let newPage, canPrev, canNext;
    switch (action.type) {
        case PagingActions.INIT:
            return {
                ...state,
                ...action.payload
            }
        case PagingActions.SET_VALUE:
            return {
                ...state,
                [action.payload.key]: action.payload.value
            }
        case PagingActions.NEXT_PAGE:
            newPage = page + 1 < Math.ceil(total / size) ? page + 1 : page;
            return {
                ...state,
                page: newPage
            }
        case PagingActions.PREVIOUS_PAGE:
            newPage = page - 1 > 0 ? page - 1 : page;
            return {
                ...state,
                page: newPage
            }
        default:
            return state;
    }
}

export function usePaging(
    initialState?: PagingState
): [
        PagingState,
        (key: 'page' | 'size' | 'total', value: number) => void,
        () => void,
        () => void
    ] {
    const [state, dispatch] = useReducer(pagingReducer, initialState || initialPagingState);
    return [
        state,
        (key: 'page' | 'size' | 'total', value: number) => setValue(dispatch, key, value),
        () => prevPage(dispatch),
        () => nextPage(dispatch)
    ];
}