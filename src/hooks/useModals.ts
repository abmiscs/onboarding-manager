import { useReducer } from "react"

enum ModalsActions {
    OPEN = 'OPEN',
    CLOSE = 'CLOSE',
    TOGGLE = 'TOGGLE'
}

type ModalsAction =
    { type: ModalsActions.OPEN, payload: { modal: string } }
    | { type: ModalsActions.CLOSE, payload: { modal: string } }

type ModalsState = {
    [key: string]: boolean
}

const openModal = (dispatch: React.Dispatch<ModalsAction>, modal: string) => {
    return dispatch({
        type: ModalsActions.OPEN,
        payload: { modal }
    })
}

const closeModal = (dispatch: React.Dispatch<ModalsAction>, modal: string) => {
    return dispatch({
        type: ModalsActions.CLOSE,
        payload: { modal }
    })
}

// const toggleModal = (dispatch: React.Dispatch<ModalsAction>, modal: string) => {
//     return dispatch({
//         type: ModalsActions.TOGGLE,
//         payload: { modal }
//     })
// }

// const setModal = (dispatch: React.Dispatch<ModalsAction>, modal: string, value: boolean) => {
//     return dispatch({
//         type: ModalsActions.SET,
//         payload: { modal, value }
//     })
// }

function modalsReducer<ModalsState>(state: ModalsState, action: ModalsAction): ModalsState {
    const { modal } = action.payload;
    switch (action.type) {
        case ModalsActions.OPEN:
            return {
                ...state,
                [modal]: true
            }
        case ModalsActions.CLOSE:
            return {
                ...state,
                [modal]: false
            }
        default:
            return state;
    }
}

export function useModals<ModalsState>(
    initialState: ModalsState
): [
        ModalsState,
        (modal: string) => void,
        (modal: string) => void
    ] {
    const [state, dispatch] = useReducer(modalsReducer, initialState);
    const exportState = state as ModalsState;
    return [
        exportState,
        (modal: string) => openModal(dispatch, modal),
        (modal: string) => closeModal(dispatch, modal)
    ]
}