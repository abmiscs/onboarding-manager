import { useReducer } from "react"

// Definisco i valori delle azioni
enum LoadingActions {
    TOGGLE_LOADING = 'TOGGLE_LOADING',
    SET_ERROR = 'SET_ERROR',
    SET_LOADING = 'SET_LOADING'
}

// Definisco le possibile azioni
type LoadingAction =
    { type: LoadingActions.TOGGLE_LOADING, payload: { loadingMsg?: string } }
    | { type: LoadingActions.SET_ERROR, payload: { loadingError: string } }
    | { type: LoadingActions.SET_LOADING, payload: { loading: boolean, loadingMsg?: string } }

// Definisco il tipo di stato
type LoadingState = {
    loading: boolean;
    loadingMsg?: string,
    loadingError?: string
}

// Oggetto di stato iniziale
const initialLoadingState: LoadingState = {
    loading: false
}

// Helper function che forza l'implementazione dell'azione specifica
const toggleLoading = (dispatch: React.Dispatch<LoadingAction>, loadingMsg?: string) => {
    return dispatch({
        type: LoadingActions.TOGGLE_LOADING,
        payload: { loadingMsg }
    })
}

// Helper function come sopra
const setLoading = (dispatch: React.Dispatch<LoadingAction>, loading: boolean, loadingMsg?: string) => {
    return dispatch({
        type: LoadingActions.SET_LOADING,
        payload: { loading, loadingMsg }
    })
}

// Helper function come sopra
const setLoadingError = (dispatch: React.Dispatch<LoadingAction>, loadingError: string) => {
    return dispatch({
        type: LoadingActions.SET_ERROR,
        payload: { loadingError }
    })
}

// Definizione reducer
const loadingReducer = (state: LoadingState = initialLoadingState, action: LoadingAction): LoadingState => {
    let newLoading;
    switch (action.type) {
        case LoadingActions.TOGGLE_LOADING:
            newLoading = !state.loading
            return {
                ...state,
                loading: newLoading,
                loadingMsg: action.payload.loadingMsg || '',
                loadingError: newLoading ? '' : state.loadingError // se loading è a true non ha senso avere un error
            }
        case LoadingActions.SET_LOADING:
            return {
                ...state,
                loading: action.payload.loading,
                loadingMsg: action.payload.loadingMsg || ''
            }
        case LoadingActions.SET_ERROR:
            return {
                ...state,
                loading: false,
                loadingMsg: '',
                loadingError: action.payload.loadingError
            }
        default:
            return state;
    }
}

export function useLoading(): [
    LoadingState,
    (loadingMsg?: string) => void,
    (error: string) => void,
    (loading: boolean, loadingMsg?: string) => void
] {
    const [state, dispatch] = useReducer(loadingReducer, initialLoadingState);
    return [
        state,
        (loadingMsg?: string) => toggleLoading(dispatch, loadingMsg),
        (error: string) => setLoadingError(dispatch, error),
        (loading: boolean, loadingMsg?: string) => setLoading(dispatch, loading, loadingMsg)
    ]
}