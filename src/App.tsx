import React, { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';

import './App.scss';

import Root from './pages/Root/Root';
import FallbackComponent from './components/UI/WaitingComponent/FallbackComponent';
import { AuthProvider } from './context/AuthProvider';

const App: React.FC = () => {

  return (
    <BrowserRouter basename={`${process.env.REACT_APP_BASENAME}`}>
      <Suspense fallback={FallbackComponent()}>
        <AuthProvider>
          <Root />
        </AuthProvider>
      </Suspense>
    </BrowserRouter >
  )

}

export default App;
