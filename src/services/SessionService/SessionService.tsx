import axios from "axios";

import { API } from "../../config/constants";
import { ApiResponse } from "../../config/types";
import { Session, OnboardingSession, Paging, Sorting } from "../../models/Models";

export type GetSessionsResponse = {
    results: Session[],
    total: number
}

export default class SessionService {

    public static async getSessions(paging?: Paging, sorting?: Sorting): Promise<ApiResponse<GetSessionsResponse>> {
        const res = await axios.post<ApiResponse<GetSessionsResponse>>(`${API.configService.getSessions}`, { paging, sorting });
        return res.data;
    }

    public static async getOnboardig(sessionId: string): Promise<ApiResponse<OnboardingSession>> {
        const res = await axios.get<ApiResponse<OnboardingSession>>(`${API.configService.getOnboarding}/${sessionId}`);
        return res.data;
    }

    public static async confirmSessions(sessionId: string): Promise<ApiResponse<any>> {
        const res = await axios.get<ApiResponse<any>>(`${API.configService.confirmSession}/${sessionId}`);
        return res.data;
    }

    public static async deleteSession(sessionId: string): Promise<ApiResponse<any>> {
        const res = await axios.delete<ApiResponse<any>>(`${API.configService.deleteSession}/${sessionId}`);
        return res.data;
    }

}