import axios from "axios";

import { API } from "../../config/constants";

import { ApiResponse } from "../../config/types";
import { Document, Paging, Sorting } from "../../models/Models";

export default class DocumentService {

    public static async getDocuments(sessionId: string, paging?: Paging, sorting?: Sorting): Promise<ApiResponse<Document[]>> {
        let result: ApiResponse<Document[]> = {};
        try {
            const res = await axios.post(`${API.documentsService.getDocuments}/`, { sessionId, paging, sorting });
            return res.data;
        } catch (error) {
            console.log('[DocumentService#getDocuments]', error);
        }
        return result;
    }

}