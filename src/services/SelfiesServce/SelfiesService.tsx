import axios from "axios";
import { API } from "../../config/constants";
import { ApiResponse } from "../../config/types";
import { Selfies, Paging, Sorting } from "../../models/Models";

export type GetSelfiesResponse = {
    results: Selfies[],
    total: number
}

export default class SelfiesService {

    public static async getSelfies(sessionId: string, paging?: Paging, sorting?: Sorting): Promise<ApiResponse<Selfies[]>> {
        let result: ApiResponse<Selfies[]> = {};
        try {
            const res = await axios.get(`${API.livenessService.getSelfies}/${sessionId}`);
            return res.data;
        } catch (error) {
            console.log('[SelfiesService#getSelfies]', error);
        }
        return result;
    }

    public static async getSelfiesCriteria(sessionId: string, paging?: Paging, sorting?: Sorting): Promise<ApiResponse<GetSelfiesResponse>> {
        let result: ApiResponse<GetSelfiesResponse> = {};
        try {
            const res = await axios.post(`${API.livenessService.getSelfies}`);
            return res.data;
        } catch (error) {
            console.log('[SelfiesService#getSelfiesCriteria]', error);
        }
        return result;
    }

    public static async getCurrentSelfies(sessionId: string): Promise<ApiResponse<GetSelfiesResponse>> {
        let result: ApiResponse<GetSelfiesResponse> = {};
        try {
            const res = await axios.get(`${API.livenessService.getCurrentSelfies}/${sessionId}`);
            return res.data;
        } catch (error) {
            console.log('[SelfiesService#getSelfiesCriteria]', error);
        }
        return result;
    }

}