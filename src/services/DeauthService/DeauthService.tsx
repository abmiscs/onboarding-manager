import axios from "axios";

import { API } from "../../config/constants";

import { ApiResponse } from "../../config/types";

export type LoginResponse = {
    authenticated: boolean;
    authtoken: string;
}

export default class DeauthService {

    public static async login(alias: string, password: string): Promise<ApiResponse<LoginResponse>> {
        let result: ApiResponse<LoginResponse> = {};
        try {
            const res = await axios.post(`${API.identityService.authenticate}/`, { alias, password });
            return res.data;
        } catch (error) {
            console.log('[DeauthService#login]', error);
        }
        return result;
    }

}